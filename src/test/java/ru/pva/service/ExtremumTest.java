package ru.pva.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by General on 4/13/2017.
 */
public class ExtremumTest {
    private Extremum extremum;
    @Before
    public void setUp() throws Exception {
        extremum = new ExtremumImpl();
    }

    @After
    public void tearDown() throws Exception {
        extremum = null;
    }

    @Test
    public void findExtremum() throws Exception {
        //arrange
        int length = 10;
        int etalonMin = 1;
        int etalonMax = length;
        Integer[] array = new Integer[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = i + 1;
        }
        Integer min = new Integer(Integer.MAX_VALUE);
        Integer max = new Integer(Integer.MIN_VALUE);


        //act
        Extremum.ExtremumValue extremumValue = extremum.findExtremum(array);

        //assert
        org.junit.Assert.assertEquals(Integer.valueOf(etalonMin), Integer.valueOf(extremumValue
                .getMin()));
        org.junit.Assert.assertEquals(Integer.valueOf(etalonMax), Integer.valueOf(extremumValue
                .getMax()));
    }

}