package ru.pva.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Класс для генерации коллецкций, заполненных случайными числами
 */
public class ArrayGenerator {
    /**
     * Генерирует коллекцию случайных целых чисел
     * @param count - задает количество генерируемых элементов
     * @param bound - диапазон значений эл-ов
     * @return - коллекцию случайных целых чисел
     */
    public static ArrayList<Integer> generateRandomCollection(int count, int bound){
        Integer[] ints = new Integer [ count ];
        Arrays.parallelSetAll( ints,
                index -> ThreadLocalRandom.current().nextInt( bound) );
        return new ArrayList<>(Arrays.asList(ints));

    }

    /**
     * Генерирует коллекцию случайных целых чисел
     * @param count - задает количество генерируемых элементов
     * @param bound - диапазон значений эл-ов
     * @return - коллекцию случайных целых чисел
     */
    public static Integer[] generateRandomArray(int count, int bound){
        Integer[] ints = new Integer [ count ];
        Arrays.parallelSetAll( ints,
                index -> ThreadLocalRandom.current().nextInt( bound) );
        return ints;

    }

}
