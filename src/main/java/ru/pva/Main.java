package ru.pva;

import ru.pva.service.Extremum;
import ru.pva.service.ExtremumImpl;
import ru.pva.utils.ArrayGenerator;

/**
 * Created by General on 4/12/2017.
 */
public class Main {
    private static final int ELEMENT_COUNT = 10_000_000;
    private static final int BOUND = 1_000_000_000;


    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        Integer[] array = ArrayGenerator.generateRandomArray(ELEMENT_COUNT, BOUND);
        long generateTime = System.currentTimeMillis() - time;
        Extremum extremum = new ExtremumImpl();


        Extremum.ExtremumValue extremumValue = extremum.findExtremum(array);
        long extremumTime = System.currentTimeMillis() - time - generateTime;

        System.out.println("MAX = " + extremumValue.getMax());
        System.out.println("MIN = " + extremumValue.getMin());
        System.out.println("Array generation time = " + generateTime + " ms");
        System.out.println("Extremum find time = " + extremumTime + " ms");
        System.out.println("General execution time = " + (generateTime + extremumTime) + " ms");
    }
}
