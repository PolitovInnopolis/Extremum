package ru.pva.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Класс реализующий интерфейс поиска min и max элемента с помощью stream API
 */
public class ExtremumImpl implements Extremum {

    private int findMax(List<Integer> list){
        return list.parallelStream().max(Integer::compareTo).get();
    }

    private int findMin(List<Integer> list){
        return list.parallelStream().min(Integer::compareTo).get();
    }

    @Override
    public ExtremumValue findExtremum(Integer[] array) {
        return new ExtremumValue(findMax(array), findMin(array));

    }

    @Override
    public int findMax(Integer[] array) {
        return findMax(new ArrayList<>(Arrays.asList(array)));
    }

    @Override
    public int findMin(Integer[] array) {
        return findMin(new ArrayList<>(Arrays.asList(array)));
    }
}
