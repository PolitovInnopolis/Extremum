package ru.pva.service;

/**
 * Интерфейс описывающий поиск максимального и минимального числа в массиве
 */
public interface Extremum {
    class ExtremumValue{
        private int max;
        private int min;

        public ExtremumValue(int max, int min) {
            this.max = max;
            this.min = min;
        }

        public int getMax() {
            return max;
        }

        public int getMin() {
            return min;
        }
    }

    /**
     * Поиск экстремумов(max) в неотсортированном массиве
     * @param  - неотсортированный массив
     * @return
     */
    ExtremumValue findExtremum(Integer[] array);


    /**
     * Поиск экстремумов(max) в неотсортированном массиве
     * @param array - массив
     * @return
     */
     int findMax(Integer[] array);

    /**
     * Поиск экстремумов(min) в неотсортированном массиве
     * @param array - массив
     * @return
     */
     int findMin(Integer[] array);
}
